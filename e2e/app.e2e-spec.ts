import { CobaltTestPage } from './app.po';

describe('cobalt-test App', () => {
  let page: CobaltTestPage;

  beforeEach(() => {
    page = new CobaltTestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
