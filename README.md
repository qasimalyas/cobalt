# CobaltTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.0.

## Notes about test

Last time I worked with Angular was a version 1.0 and wow has this thing changed.
I've done what I could but i still feel that some improvements can be made. The instructions below should be enough to get this up and running but I wanted to make some important points during its review.

Some things that I wanted to do and also had issues with were form validation. I recognise this isn't there but that doesn't mean I ignored it or I wasn't cognisant of it. Its an important for UX and accessibility and I wish I had more experience with Angular to add this.

As this is a simple app I didn't break it up to any components. It’s just a form, textbox button and a feedback chunk on the page. On a more complicated example I would split this in more maintainable components and import them where needed.

I personally feel that I didn't really make full use of Angular to achieve the test objective but hopefully with collaborating with more experienced developers I would pick this very quickly.

Many thanks for reading this.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
