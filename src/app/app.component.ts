import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  inputHistory:any = [];
  limit:number = 10;
  target;

  ngAfterViewInit(){
    this.target = document.querySelector('#history');
    this.showOnFrontEnd()
  }

  onFormSubmit(area) {
    if(area.value.length > 0) {
      this.splitText(area.value);
    }
  }

  splitText(copy:string) {
    var splitCopy = copy.split(/\s+/);
    var convertText = this.changeIt(splitCopy);
    if(convertText[0] === undefined) return;
    this.saveHistory(convertText);
  }

  changeIt(text) {
    var conversion = text.map(function(word) {
      if(!word.match(/[a-zA-Z]+/gi)) return;
      if (word.slice(0,1).match(/[aeiou]/gi)) {
        var first = word.slice(0,1);
        word = word.slice(1)+first + 'ay'
      }
      else {
        word = word+'way';
      }
      return word
    });
    return conversion;
  }

  saveHistory(output) {
    let userHistory = this.inputHistory;
    userHistory.unshift(output);
    let slicedArr = userHistory.slice(0, this.limit);
    this.saveToLocalStorage(slicedArr);
    this.showOnFrontEnd();
  }

  saveToLocalStorage(historyObj) {
    localStorage.setItem('history', JSON.stringify(historyObj));
  }

  showOnFrontEnd() {
    if(!localStorage.getItem('history')) return;
    this.target.innerHTML = '';
    let arr = JSON.parse(localStorage.getItem('history'));
    // i could have also used a an angular ng loop to do 
    // list generation but my stored data isn't flat so 
    // this approach seemed to be easier.
    arr.map(item => {
      var list = document.createElement('li');
      list.innerHTML = item.join(' ');
      this.target.appendChild(list)
    });
  }

}
